from constants import *
from messages import *

def makesong(song, s):
        """ Creates a .song file corresponding to a Song object """

        info(s, 1, "Generating song file")
        
        output = ""

        for label in METADATA:
                if label in song.metadata:
                        output += label + " " + song.metadata[label] + "\n"

        output += "automatic-structure off\n\n"

        for section_name in song.sections:
                section = song.sections[section_name]
                output += "\n\nsection " + section_name + "\n"
                for bar in section.bars:
                        output += "\tduration " + str(bar.duration) + "\n"
                        output += "\tswing " + str(bar.swing) + "\n"
                        output += "\ttempo " + str(bar.tempo) + "\n"
                        output += "\tnew-bar" + "\n"
                        for txch in bar.content:
                                output += "\t\tchord " + str(txch.chord) + "\n"
                                output += "\t\tduration " + str(txch.duration) + "\n"
                                output += "\t\ttext " + ("~" if txch.delay else "") + txch.text + ("-" if not txch.split else "") + "\n"
                                output += "\tadd-to-bar\n"
                        output += "\tbar-complete\n\n"

        for section_name in song.structure:
                output += "play " + section_name + "\n"

        output_path = s.output_folder + s.song_name + ".new.song"
        info(s, 1, "Writing song file to file: ", output_path)
        song_file = open(output_path, "w")
        song_file.write(output)
        song_file.close()
