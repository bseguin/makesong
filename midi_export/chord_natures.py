CHD_NAT                 =       {}

# Intervals
CHD_NAT["5"]            =       [0,7]
CHD_NAT["B5"]           =       [0,6]

# Basic triads
CHD_NAT[""]             =       [0,4,7]
CHD_NAT["MAJ"]          =       CHD_NAT[""]
CHD_NAT["M"]            =       [0,3,7]
CHD_NAT["MIN"]          =       CHD_NAT["M"]
CHD_NAT["DIM"]          =       [0,3,6]
CHD_NAT["AUG"]          =       [0,4,7]
CHD_NAT["+"]            =       CHD_NAT["AUG"]
CHD_NAT["#5"]           =       CHD_NAT["AUG"]
CHD_NAT["SUS"]          =       [0,5,7]
CHD_NAT["SUS4"]         =       CHD_NAT["SUS"]
CHD_NAT["SUS2"]         =       [0,2,7]

# "Majory" extended chords
CHD_NAT["6"]            =       [0,4,7,9]
CHD_NAT["ADD9"]         =       [0,2,4,7]
CHD_NAT["ADD2"]         =       CHD_NAT["ADD9"]
CHD_NAT["ADD4"]         =       [0,4,5,7]
CHD_NAT["6/9"]          =       [0,2,4,7,9]

CHD_NAT["MAJ7"]         =       [0,4,7,11]
CHD_NAT["MAJ9"]         =       [0,2,4,7,11]
CHD_NAT["MAJ13"]        =       [0,2,4,7,9,11]

CHD_NAT["MAJ7#11"]      =       [0,4,6,7,11]
CHD_NAT["MAJ9#11"]      =       [0,2,4,6,7,11]
CHD_NAT["MAJ13#11"]     =       [0,2,4,6,7,9,11]

# and their suspended friends
CHD_NAT["MAJ7SUS"]      =       [0,5,7,11]
CHD_NAT["MAJ7SUS4"]     =       CHD_NAT["MAJ7SUS"]
CHD_NAT["SUSMAJ7"]      =       CHD_NAT["MAJ7SUS"]
CHD_NAT["SUS4MAJ7"]     =       CHD_NAT["MAJ7SUS"]
CHD_NAT["MAJ7SUS2"]     =       [0,2,7,11]
CHD_NAT["SUS2MAJ7"]     =       CHD_NAT["MAJ7SUS2"]
CHD_NAT["MAJ9SUS"]      =       [0,2,5,7,11]
CHD_NAT["MAJ9SUS4"]     =       CHD_NAT["MAJ9SUS"]
CHD_NAT["SUSMAJ9"]      =       CHD_NAT["MAJ9SUS"]
CHD_NAT["SUS4MAJ9"]     =       CHD_NAT["MAJ9SUS"]

# "Dominanty" seventh chords
CHD_NAT["7"]            =       [0,4,7,10]
CHD_NAT["9"]            =       [0,2,4,7,10]
CHD_NAT["11"]           =       [0,2,4,5,7,10]
CHD_NAT["7#11"]         =       [0,4,6,7,10]
CHD_NAT["9#11"]         =       [0,2,4,6,7,10]
CHD_NAT["13"]           =       [0,2,4,7,9,10]
CHD_NAT["13#11"]        =       [0,2,4,5,6,7,10]
CHD_NAT["7B5"]          =       [0,4,6,10]
CHD_NAT["7#5"]          =       [0,4,8,10]

CHD_NAT["7B9"]          =       [0,1,4,7,10]
CHD_NAT["7#9"]          =       [0,3,4,7,10]
CHD_NAT["7B9B5"]        =       [0,1,4,6,10]
CHD_NAT["7B9#5"]        =       [0,1,4,8,10]
CHD_NAT["7#9B5"]        =       [0,3,4,6,10]
CHD_NAT["7#9#6"]        =       [0,3,4,8,10]

        # plus all the sus...

# "Minory" extended chords
CHD_NAT["M6"]           =       [0,3,7,9]
CHD_NAT["MB6"]          =       [0,3,7,8]
CHD_NAT["MADD9"]        =       [0,2,3,7]
CHD_NAT["MADD2"]        =       CHD_NAT["MADD9"]
CHD_NAT["M6/9"]         =       [0,2,3,7,9]
CHD_NAT["M7"]           =       [0,3,7,10]
CHD_NAT["M7B5"]         =       [0,3,6,10]
CHD_NAT["MMAJ7"]        =       [0,3,7,11]
CHD_NAT["M9"]           =       [0,2,3,7,10]
CHD_NAT["M9B5"]         =       [0,2,3,6,10]
CHD_NAT["MMAJ9"]        =       [0,2,3,7,11]
CHD_NAT["M9MAJ7"]       =       CHD_NAT["MMAJ9"]
CHD_NAT["M13"]          =       [0,2,3,7,9,11]

        #plus all the sus

# Other extended chords
CHD_NAT["DIM7"]         =       [0,3,6,9]
CHD_NAT["DIM7ADD9"]     =       [0,2,3,6,9]
CHD_NAT["DIM9"]         =       CHD_NAT["DIM7ADD9"]