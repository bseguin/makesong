from midiutil.MidiFile import MIDIFile
from random import choice
from math import floor
from os import system

from constants import *
from messages import *

from datatypes import Chord
from midi_export.pattern_to_pitches import pattern_to_pitches
from midi_export.parse_style import parse_style


# Time is always measured in quarter notes

def compute_swing(time, ratio):
        inttime = floor(time)
        fractime = time-inttime
        return inttime + ratio*fractime/0.5

def makemid(song, s):
        info(s, 1, "Importing style: ", s.midi_style)

        instruments = parse_style(s.midi_style)
        nb_tracks = len(instruments)
        
        info(s, 1, "Generating MIDI")
        
        mf = MIDIFile(nb_tracks)
        
        the_chords = list(song.get_chords())
        last_chord = Chord("N")

        for i in range(nb_tracks):
                instrument = instruments[i]

                mf.addTrackName(i, 0, instrument.name)
                volume = instrument.volume

                if instrument.is_perc:
                        channel = 9
                else:
                        channel = i + int(i>=9)
                        mf.addProgramChange(0, channel, 0, instrument.midi_id)

                for j in range(len(the_chords)):
                        time, time_in_bar, chord, tempo, swing = the_chords[j]
                        if chord == None or chord.special in [CHORD_UNDEF, CHORD_REPEAT]:
                                chord = last_chord
                        mf.addTempo(i, 0, tempo)
                        next_chord = None
                        duration = 4
                        if j < len(the_chords) - 1:
                                duration, _, next_chord, _, _ = the_chords[j+1]
                                duration -= time
                        pattern = choice(instrument.patterns)
                        pitches_in_pattern = list(pattern_to_pitches(chord, pattern, next_chord))
                        for k in range(int(4*duration)):
                            # Uncomment if you want every chord to restart the pattern:
                            #pattern_k = k%16
                            pattern_k = int((k + time_in_bar*4)%16)
                            pitches_played = pitches_in_pattern[pattern_k]
                            play_time = compute_swing(time + k/4, swing)
                            for pitch in pitches_played:
                                if instrument.is_perc:
                                        mf.addNote(i, channel, pitch, play_time, 0.25, volume)
                                else:
                                        pitch += s.global_transpose
                                        pitch += instrument.octave_shift * 12
                                        mf.addNote(i, channel,
                                                   pitch,
                                                   play_time,
                                                   0.25,
                                                   volume)
                        last_chord = chord

        output_path = s.output_folder + s.song_name + ".mid"
        info(s, 1, "Writing MIDI to file: ", output_path)
        mid_file = open(output_path, 'wb')
        mf.writeFile(mid_file)
        mid_file.close()

        if s.midi_play:
                info(s, 1, "Playing MIDI file")
                system(MIDI_PLAY_COMMAND + output_path)
