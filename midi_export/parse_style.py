from datatypes import Instrument, Percussion

def parse_style(filename):
        style_file = open(filename, "r")
        lines = style_file.readlines()
        style_file.close()

        instruments = []
        
        for line in lines:
                line = line.split(";")[0]                       # remove comments
                args = line.lower().strip().split()             # break down line
                if len(args) == 0:
                        continue
                command = args[0]
                fullarg = ' '.join(args[1:])
                if command == "instrument":
                        instrument_name = fullarg
                        instruments.append(Instrument(instrument_name, 0, [], 100))
                elif command == "percussion":
                        instrument_name = fullarg
                        instruments.append(Percussion(instrument_name, [], 100))
                elif command == "midi" and len(args) >= 2:
                        midi_id = int(args[1])
                        instruments[-1].midi_id = midi_id
                elif command == "volume" and len(args) >= 2:
                        volume = int(args[1])
                        instruments[-1].volume = volume
                elif command == "octave" and len(args) >= 2:
                        octave_shift = int(args[1])
                        instruments[-1].octave_shift = octave_shift
                elif command == "pattern" and len(args) >= 2:
                        args = (args[1:]*16)[:16]                   # makes sure this is size 16
                        pattern = [int(arg) for arg in args]
                        instruments[-1].patterns.append(pattern)

        return instruments
