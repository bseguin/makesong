from midi_export.chord_natures import *

# A rythmic pattern is a sequence of 16 integers among:
#       -1 = hold
#       0 = silence
#       1 = root
#       2 = second
#       3 = third etc.
#       ...
#       7 = seventh
#       8 = whole chord
#       9 = rootless, fifthless voicing
#       10 = basic triad w/ correct bass
#       11 = bass
#       12 = bass, an octave higher
#       13 = fifth if bass=root, root otherwise
#       14 = same as 13 but an octave lower
#       15 = same as 13 but an octave higher
#       16 = note one semitone below the bass
#       17 = note one semitone below the bass of the next chord
#       18 = kick
#       19 = snare
#       20 = closed hihat
#       21 = open hihat
#       22 = clap
#       23 = ride
#       24 = crash
#       25 = clave

def inn(pitch, pitch_set):
    """ Checks if a pitch is in pitch_set, up to octave equivalence (i.e. mod 12) """
    return pitch % 12 in [i % 12 for i in pitch_set]
    
def first_inn(pitches, pitch_set, otherwise):
    """ Returns the first element of pitches that is in pitch_set, and if none are then returns otherwise. """
    for pitch in pitches:
        if inn(pitch, pitch_set):
            return pitch
    return otherwise

def int_to_pitch(chord, figure, next_chord=None):
        """ Returns a list of midi pitches """

        root = chord.root.pitch + 60
        bass = chord.bass.pitch + 48
        nature = chord.nature.upper().strip()
        intervals = CHD_NAT[nature]

        chord_pitches = [bass] + [root + i for i in intervals]

        if figure == 0:
                return []
        elif figure == 1:
                return [root]
        elif figure == 2:
                # Major 2nd except if there is a b9 and no 9 (TOFIX: may be a problem for 7#9 chords)
                return [first_inn([root+2, root+1], chord_pitches, root+2)]
        elif figure == 3:
                # Major 3rd except if there is a m3 and no Maj3
                return [first_inn([root+4, root+3], chord_pitches, root+4)]
        elif figure == 4:
                # Perfect 4th except if there is a #4/#11 and no 4
                return [first_inn([root+5, root+6], chord_pitches, root+5)]
        elif figure == 5:
                # Perfect 5th except if there is a b5 or #5 and no 5 (is this the desired behaviour?)
                return [first_inn([root+7, root+6, root+8], chord_pitches, root+7)]
        elif figure == 6:
                # Major 6th except if there is a b6 and no 6 (is this the desired behaviour, esp. for minor chords?)
                return [first_inn([root+9, root+8], chord_pitches, root+9)]
        elif figure == 7:
                # Finding a probable seventh is tricky (without knowing the key especially, since we don't know whether we have a I, a IV or a V with major triads)
                if inn(root+11, chord_pitches):                 # Major 7th if it is present
                        return [root+11]
                if inn(root+10, chord_pitches):                 # Minor 7th if it is present
                        return [root+10]
                if inn(root+3, chord_pitches) and not inn(root+4, chord_pitches):       # If there is a minor 3rd, go with the m7
                        return [root+10]
                if inn(root+1, chord_pitches) or inn (root+6, chord_pitches):           # If there is a b9 or spicy #11/b5, assume it's dominant
                        return [root+10]
                return [root+11]                                        # In other situations, assume it's Maj7,
                                                                        # Dominant 7 w/ major 7th is a more forgivable mistake that the opposite one.

        elif figure == 8:
                return chord_pitches
        elif figure == 9:
                return [pitch for pitch in chord_pitches if not inn(pitch, [root, bass, (root+7)])]
        elif figure == 10:
                result = [bass, root]
                result.append(first_inn([root+4,root+3,root+5,root+2,root+1], chord_pitches, root+2)) # Add Maj3, 3, sus4, sus2, susb2? or in doubt the ninth
                result.append(int_to_pitch(chord, 5)[0])
                return result
        elif figure == 11:
                return [bass]
        elif figure == 12:
                return [bass+12]
        elif figure in [13, 14, 15]:
                intended_pitch = root
                if root%12 == bass%12:
                        intended_pitch = int_to_pitch(chord, 5)[0]
                intended_pitch = bass + (intended_pitch - bass)%12        # Take the lowest one above the bass
                if figure == 14:
                    intended_pitch -= 12
                elif figure == 15:
                    intended_pitch += 12
                return [intended_pitch]
        elif figure == 16:
                return [bass-1]
        elif figure == 17:
                nextbass = root+5                                       # Assume movement by 4th if you have no other info (I mean why not)
                if next_chord != None and next_chord.bass != None:
                        nextbass = next_chord.bass.pitch + 48
                return [nextbass-1]
        elif figure >= 18 and figure <= 25:
                return [[35, 38, 42, 46, 39, 51, 49, 75][figure-18]]    # Handles midi pitches for percussion
        else:
            assert False, "Unexpected figure in pattern: '" + str(figure) + "'."

def pattern_to_pitches(chord, pattern, next_chord=None):
        """ Generates lists of midi pitches happening at the same sixth note. """

        if chord == None or chord.root == None:
                return []

        for figure in pattern:
                result = []
                for pitch in int_to_pitch(chord, figure, next_chord):
                        result.append(pitch)
                yield result
