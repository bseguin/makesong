from constants import *
from datatypes import *
from messages import *
from song_parse.minisong import minisong

import re

#TODO : Use // for comments, because ; is useful
#TODO : Refactor parsing: sequentially read characters as they go instead of splitting everything, have a parse_chord subfunc

class FromMemory():
        """ Stores the information needed to be able to perform chords-from and loop-from. """
        section = None                  # Section whose chords are taken from
        loop = False
        bar_counter = -1                # Counts elapsed bars within the section
        chord_counter = 0               # Counts elapsed chords within the bar

        def reset(self):
                self.section = None
                self.loop = False
                self.bar_counter = -1
                self.chord_counter = 0

        def inc_bar(self):
                if self.section == None:
                        return

                self.bar_counter += 1
                self.chord_counter = 0

                if self.bar_counter >= len(self.section.bars):  # Handle loop-from
                        if self.loop:
                                self.bar_counter %= len(self.section.bars)
                        else:
                                self.reset()

        def inc_chord(self):
                self.chord_counter += 1

        def get_current_bar(self):
                """ Returns the bar that bar_counter currently points to. """
                if self.section == None:
                        return None
                if self.bar_counter >= len(self.section.bars):
                        return None

                return self.section.bars[self.bar_counter]
                

        def get_current_textchord(self):
                """ Returns the chord that the two counters currently point to. """
                bar = self.get_current_bar()
                if bar == None or self.chord_counter >= len(bar.content):
                        return None
                else:
                        return bar.content[self.chord_counter]


def format_command_line(line):
        """ Removes comments, strips, and removes multiple spaces """
        args = line.split(";")[0].strip().split()
        return ' '.join(args)
    

def parse_bar(bar_text):
        """ Generates the minisong code corresponding to consecutive bars. """

        if len(bar_text) == 0:                  # "Empty" bars (not even a space, i.e. "||") don't count
                yield from []

        duration = "?"
        if bar_text[0] == "{":                  # Handle custom duration
                huh = bar_text.split("}")
                duration = float(huh[0][1:])
                bar_text = "}".join(huh[1:])

        yield("duration", duration)
        yield("new-bar", "")

        by_chord = re.split('[`(]', bar_text)

        if bar_text.strip() != "" and bar_text.strip()[0] == "(":
                by_chord = by_chord[1:]         # Ignore "blank" text that precedes the first chord if specified by "("

        for textchord_text in by_chord:
                text = textchord_text
                chord_text = None
                chord_duration = "?"

                if ")" in textchord_text:       # Handle custom chord
                        chord_text, text = text.split(")")[0:2]
                        if ":" in chord_text:   # Handle custom duration
                                chord_text, chord_duration = chord_text.split(":")[0:2]
                                chord_duration = float(chord_duration)
                
                if chord_text != None:
                        yield("chord", chord_text)                      
                else:
                        yield("chord", "")
                yield ("duration", chord_duration)
                yield ("text", text)
                yield ("add-to-bar","")          # Adds the text/chord to the bar

        yield ("bar-complete", "")       # Computes the durations for chords of unspecified duration in the bar


def parse_songfile(s):
        song = Song()

        song_path = s.song_file
        song_file = open(song_path, "r")
        file_content = song_file.readlines()
        song_file.close()

        rgstr = {}      # Creating the register: this will store the current context during the parsing

        # Default values
        default_section = Section()
        rgstr["section"] = default_section

        rgstr["tempo"] = 110
        rgstr["time"] = (4, 4)
        rgstr["swing"] = 0.5
        rgstr["transpose"] = 0
        rgstr["split-words"] = "default"

        automatic_structure = True

        # Starting values
        rgstr["chord"] = Chord("N")
        rgstr["from-mem"] = FromMemory()


        current_line_nb = 0

        while current_line_nb < len(file_content):
                line = format_command_line(file_content[current_line_nb])
                current_line_nb += 1

                args = line.split()

                if args == []:
                        continue

                arg_nb = len(args)
                command = args[0].lower()
                arg_full = ' '.join(args[1:])

                if command in METADATA and arg_nb >= 2:
                        song.metadata[command] = arg_full

                elif command in MINISONG_CMD:
                        minisong(command, arg_full, rgstr)

                elif command == "automatic-structure" and arg_nb >= 2:
                        if args[1].lower() in ["on", "default"]:
                                automatic_structure = True
                        elif args[1].lower() == "off":
                                automatic_structure = False
                
                elif (command in ["section", "mute-section"]) and arg_nb >= 2:
                        rgstr["section"] = Section()
                        rgstr["from-mem"].reset()
                        info(s, 2, "Parsing section ", arg_full)
                        song.sections[arg_full] = rgstr["section"]
                        if command == "section" and automatic_structure:
                                song.structure.append(arg_full)

                elif command == "play" and arg_nb >= 2:
                        song.structure.append(arg_full)

                elif command == "repeat" and arg_nb >= 3:
                        N = int(args[1])
                        section_name = ' '.join(args[2:])
                        song.structure += N * [section_name]

                elif (command in ["chords-from", "loop-from"]) and arg_nb >= 2:
                        rgstr["from-mem"].reset()
                        rgstr["from-mem"].section = song.sections[arg_full]
                        rgstr["from-mem"].loop = (command == "loop-from")

                elif command=="define" and arg_nb >= 3:
                        #TODO
                        continue

                elif command=="define-absolute" and arg_nb >= 3:
                        #TODO
                        continue

                elif line[0] == "|":
                        line = line[1:]      # Stripping the first | ; thus | has the strict meaning of "bar end".
                        bar_text = ""
                        continue_reading = True

                        while continue_reading:
                            inchord = False
                            for index, char in enumerate(line):
                                inchord = (char == '(') or (inchord and char != ')')
                                if char not in ['|','/'] or inchord:
                                    bar_text += char
                                else:
                                    if index == len(line)-1:            # if bar end happens at eol
                                        continue_reading = False
                                        if char == "|":
                                            bar_text += NEWLINE_MARKER
                                    info(s, 3, "Generating minisong code for bar:", "\n\t" + bar_text)
                                    info(s, 4, "Executing that minisong code...")
                                    for command, arg in parse_bar(bar_text):
                                        print_minisong_code(s, command, arg)
                                        minisong(command, arg, rgstr) # Execution
                                    bar_text = ""
                            # reached eol
                            if bar_text != "":                      # if eol occured during a bar:
                                bar_text += NEWLINE_MARKER          # insert a newline marker
                                assert current_line_nb < len(file_content), "Reached eof in the middle of a bar, please close bars with |"
                                line = format_command_line(file_content[current_line_nb])   # start reading the next line
                                current_line_nb += 1
                            else:
                                continue_reading = False

                else:
                        error(s, 0, "Unrecognized command " + colors.BOLD + command + colors.ENDC + " called with arguments \"" + colors.BOLD + str(arg_full) + colors.ENDC +"\"")
                        if s.verbose >= 2:
                                input("Press ENTER to resume.")
        return song
