from datatypes import *

""" Minisong is the small register-based interpreter at the heart of the parser;
    Chords and text are transpored into miniparser instructions which are then executed by the interpreter.  """

def format_lyrics(lyric, split_words_nospace):
        delay, split = False, True
        if lyric.strip() == "":
                return "", False, True
        if not split_words_nospace:
                lyric = lyric.strip()
                if lyric[0] == "~":
                        delay = True
                        lyric = lyric[1:]
                if lyric[-1] == "-" and (len(lyric)==1 or lyric[-2]!=" "):
                        split = False
                        lyric = lyric[:-1]
        else:
                if lyric[0] == " ":
                        delay = True
                if lyric[-1] != " ":
                        split = False
        return lyric.strip(), delay, split


def process_duration(rgstr, has_to_answer=False):
        #TODO Handle fractions
        duration = rgstr["duration"]
        if isinstance(duration, float):
                return
        elif duration.strip() == "?":
                if rgstr["from-mem"].section != None:
                        from_bar = rgstr["from-mem"].get_current_bar()
                        if from_bar != None:
                                duration = from_bar.duration
                elif has_to_answer:
                        if rgstr["time"] == None:
                                duration = 4
                        else:
                                a, b = rgstr["time"]
                                duration = a*4/b
                else:
                        duration = "?"
        else:
                duration = float(duration)
        rgstr["duration"] = duration

def minisong(command, arg, rgstr):
        if command == "tempo":
                rgstr["tempo"] = int(arg)
        elif command == "time":
                args = arg.lower().split()
                if args[0] == "free":
                        rgstr["time"] = None
                else:
                        time_division = 4 if len(args) == 1 else int(args[1])
                        rgstr["time"] = (int(args[0]), time_division)
        elif command == "split-words":
                rgstr["split-words"] = arg.strip().lower()
        elif command == "swing":
                if isinstance(arg, float):
                        rgstr["swing"] = arg
                elif isinstance(arg, str):
                        if arg.lower().strip() == "off":
                                rgstr["swing"] = 0.5
                        elif arg.lower().strip() == "on":
                                rgstr["swing"] = 0.67
                        else:
                                rgstr["swing"] = float(arg)
        elif command=="transpose":
                rgstr["transpose"] = int(arg)
        elif command == "chord":
                if isinstance(arg, Chord):
                        rgstr["chord"] = arg
                else:
                        rgstr["chord"] = Chord(arg)
        elif command == "text":
                rgstr["text"], rgstr["delay"], rgstr["split"] = format_lyrics(arg, rgstr["split-words"] == "nospace")
        elif command == "duration":
                rgstr["duration"] = arg
        elif command == "add-to-bar":
                new_textchord = TextChord()

                register_chord = rgstr["chord"]
                from_txch = rgstr["from-mem"].get_current_textchord()
                if (register_chord == None or register_chord.special == CHORD_UNDEF) and (from_txch != None and from_txch.chord != None):
                        new_textchord.chord = from_txch.chord
                        new_textchord.duration = from_txch.duration
                else:
                        new_textchord.chord = register_chord
                        process_duration(rgstr, has_to_answer=False)
                        new_textchord.duration = rgstr["duration"]

                if new_textchord == None:
                        new_textchord = Chord()
                new_textchord.chord = new_textchord.chord.transpose(rgstr["transpose"])
                new_textchord.text = rgstr["text"]
                new_textchord.delay = rgstr["delay"]
                new_textchord.split = rgstr["split"]
                rgstr["bar"].content.append(new_textchord)
                rgstr["from-mem"].inc_chord()
        elif command == "new-bar":
                rgstr["from-mem"].inc_bar()
                process_duration(rgstr, has_to_answer=True)
                rgstr["bar"] = Bar(rgstr["duration"], rgstr["tempo"], rgstr["swing"])
                rgstr["section"].bars.append(rgstr["bar"])
        elif command == "bar-complete":
                rgstr["bar"].compute_durations()
        else:
                print("Huh? Minisong received unrecognized command", command)
                input()
