# MakeSong : A song description language


## Description

*MakeSong* is a program that deals with abstract representations of some
aspects of a song, described in a *.song* file. It is mostly designed to output
"chord sheets" and crafting songbooks for simple unrehearsed performances of
songs, with a singer accompanied by harmonic and percussive instruments.

A *.song* file describes:
- Basic information about the song (title, artist, author, composer, tempo,
  key, etc.);
- Harmony (chords) and precise harmonic rhythm (duration of every bar and
  chord);
- Text, including proper alignment of text and harmony

... but it doesn't describe exact melodic rhythm or melody lines.

Many shortcuts are offered by the *.song* format to input chords rapidly and
with minimal repetition, while focusing on text.
Transposition is also made easy.


## Usage

To run this program, one needs:
- A working *Python 3* installation;
- For LaTeX export: a complete *pdflatex* installation;
- For MIDI export: the *Python* package *midiutil*,
  as well as *fluidsynth* for playback (on Linux at least).

**[WARNING]** Settings can and should be edited in the file "*constants.py*".
              Otherwise the program is almost guaranteed not to work as
              intended on your specific computer.

The program currently supports four export options:

- **makesong.py** transforms a *.song* file into a *.tex* file, and compiles it
  into a *.pdf* file using *pdflatex*. This is the default export mode.

  The resulting *.pdf* file is intended to be practical for a simple
  guitar/piano + voice performance of the song.

  Usage:
```
python makesong.py (--latex) examples/aigle.song
```

- **makesong.py --txt** transforms a *.song* file into a *.txt* file,
  with proper alignment of text and chords.

  The resulting *.txt* file is intended to be practical for a simple
  guitar/piano + voice performance of the song in contexts where a
  *.pdf* file is unpractical (for example for sharing songs on chord
  sharing websites).

  Usage:
```
python makesong.py --txt examples/aigle.song
```

- **makesong.py --midi** transforms a *.song* file into a *.mid* file, using a
  style descriptor (located at *midi_export/default.style* by default),
  including various instruments and patterns.

  The resulting *.mid* file is probably useless for any serious musical project
  but can be used to check that the chords are correct or to practice singing
  and playing a song.

  Usage:
```
python makesong.py --midi examples/aigle.song
```

- **makesong.py --song** parses a *.song* file and transforms the Song object
  created by the parser back into a *.song* file. This is mostly useful for
  debugging the program (it lets one see the complete state of the Song
  object).

  Usage:
```
python makesong.py --song examples/aigle.song
```

Try the **-h** or **--help** option for information about all possible
parameters:
```
python makesong.py --help
```


## Documentation and the *.song* file format

To understand the *.song* format, one should probably give a look at the
example songs in the *examples/* folder. However, there are a few helpful
documents in the *doc/* folder:

- **doc/full-command-set.txt**  is an exhautive list of all commands that can
                                be used in a *.song* file. Most of these
                                commands are implementation-specific
                                ("*minisong* commands") and are useless for
                                the end user;
                                
- **doc/style-description.txt** is a quick guide to help you write
                                custom styles;

- **doc/spec.txt**              is a first attempt at a specification.

One should also check the **--help** page for details about execution options.

