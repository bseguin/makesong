### SETTINGS ###

NEWLINE_MARKER = "@NewLine@"                                                            # Can be anything that can't possibly appear "accidentally" in lyrics.

LATEX_COMPILE_COMMAND = "pdflatex "                                                     # Command to compile LaTeX files (/!\ must end with whitespace)
MIDI_PLAY_COMMAND = "fluidsynth -a pulseaudio /usr/share/soundfonts/FluidR3_GM.sf2 "    # Command to play MIDI files     (/!\ must end with whitespace)
#MIDI_PLAY_COMMAND = "vlc "

# Default folders (for output)                                                                                            /!\ FOLDERS MUST END WITH "/"
TMP_FOLDER = "tmp/"                                                                     # Temporary folder
LATEX_DEFAULT_FOLDER = TMP_FOLDER                                                       # Default folder to put .tex files in
LATEX_PDF_DEFAULT_FOLDER = "./"                                                         # Default folder to put .pdf files in
MIDI_DEFAULT_FOLDER = "./"                                                              # Default folder to put .mid files in
SONG_DEFAULT_FOLDER = "./"                                                              # Default folder to put .song files in
TXT_DEFAULT_FOLDER = "./"                                                               # Default folder to put .txt files in

class Settings:
        """ Default values. """
        export_mode = 0
        verbose = 1                                                                     # Show minisong code before execution?
        latex_template = "latex_export/template.tex"
        latex_compile = True
        midi_play = True                                        
        expand_repeat = False                                   # TODO          (%'s are replaced by the previous chord)
        show_text = True
        show_chords = True
        show_bars = False
        global_transpose = 0
        output_folder = "./"
        pdf_output_folder = LATEX_PDF_DEFAULT_FOLDER
        song_file = ""
        song_name = ""
        midi_style = "examples/styles/default.style"


### LIST OF SPECIAL COMMANDS ###

METADATA = ["title", "artist", "author", "composer",
            "year", "language", "style",
            "key", "style", "global-transpose"]

LATEX_EXPORT_SETTINGS = ["latex-mode", "latex-cols", "latex-showbars"]

METADATA += LATEX_EXPORT_SETTINGS                                                       # Not really metadata, but useful trick to recover the values at export time :)

MINISONG_CMD = ["tempo", "time", "split-words", "swing",
                "transpose", "chord", "text", "duration",
                "add-to-bar", "new-bar", "bar-complete"]


### MUSICAL CONSTANTS ###

NOTE_NAMES = "CDEFGAB"                                                                  # Names of "natural" notes (~ C major scale)
NOTE_SEMITONES = [0, 2, 4, 5, 7, 9, 11]                                                 # Corresponding n° of semitones starting from C

TRANSP_DEG_BY_SEMI = [0, 1, 1, 2, 2, 3, 3, 4, 5, 5, 6, 6]                               # How many scale degrees should you transpose for a given nb of semitones?


### ENUMS ###

CHORD_NORMAL = 0
CHORD_NC = 1                   # N.C. : Don't play any chord
CHORD_UNDEF = 2                # Undefined : default behaviour => keep playing the last chord
CHORD_REPEAT = 3               # Repeat the last chord. Same behaviour as undefined but explicit.

EXPORT_MODE_TEX = 0
EXPORT_MODE_MID = 1
EXPORT_MODE_SONG = 2
EXPORT_MODE_TXT = 3
