from sys import argv, exit
from os.path import basename

from constants import *
from datatypes import note_from_string
from song_parse.parse import parse_songfile

from latex_export.maketex import maketex
from midi_export.makemid import makemid
from song_export.makesong import makesong
from txt_export.maketxt import maketxt

from messages import *

#TODO : Not use global transpose when asking for a key. Instead, use a key and make global-transpose find a key (or make global_transpose also take a global_degree_transpose)
#TODO : add a Parameter class & produce the help message automatically along with implementation

s = Settings()
manually_defined = []
ask_key = None

def handle_parameters(parameter, args, s):
        global ask_key
        
        if parameter in ["h", "help"]:
                print("Usage: python makesong.py [OPTION] SONGFILE")
                print_twocols("", "Run MakeSong on SONGFILE, producing a .pdf/.txt/.song/.mid file depending on options.", 2)
                print("\nList of available options:")
                width = 22
                for line in [
                    (" -h, --help", "Display this help."),
                    (" -v, --verbose", "Increase verbosity."),
                    (" -q, --quiet", "Set verbosity to its minimal value."),
                    (" -l, --latex", "Set export mode to LaTeX (default)."),
                    (" -m, --midi", "Set export mode to MIDI."),
                    (" -s, --song", "Set export mode to song file."),
                    (" -t, --txt", "Set export mode to text file."),
                    (" -p, --play", "Automatically play the generated MIDI file after generation (default)."),
                    (" -P, --no-play", "Don't play the generated MIDI file after generation."),
                    (" -c, --compile", "Compile the generated tex file to pdf after generation (default)."),
                    (" -C, --no-compile", "Don't compile the generated tex file to pdf after generation."),
                    (" -b, --show-bars", "Display bar lines in tex/pdf/txt output (overwrites latex-showbars)."),
                    (" -B, --no-show-bars", "Don't display bar lines in tex/pdf/txt output (default, overwrites latex-showbars)."),
                    (" -k, --chords-text", "Display both chords and lyrics in tex/pdf/txt output (default, overwrites latex-mode)."),
                    (" -K, --chords-only", "Don't print lyrics in tex/pdf/txt output  (overwrites latex-mode)."),
                    (" -T, --text-only", "Don't print chords in tex/pdf/txt output (overwrites latex-mode)."),
                    (" --transpose VALUE", "Transposes the song by VALUE semitones."),
                    (" --key KEY", "Transposes the song to KEY (only works if the song file mentions a key).")
                ]:
                    print_twocols(line[0], line[1], width)
                exit()
        elif parameter in ["l", "latex"]:
                s.export_mode = EXPORT_MODE_TEX
                info_export_mode(s, "LaTeX / pdf")
        elif parameter in ["m", "midi"]:
                s.export_mode = EXPORT_MODE_MID
                info_export_mode(s, "MIDI file")
        elif parameter in ["s", "song"]:
                s.export_mode = EXPORT_MODE_SONG
                info_export_mode(s, "song file")
        elif parameter in ["t", "txt"]:
                s.export_mode = EXPORT_MODE_TXT
                info_export_mode(s, "text file")
        elif parameter in ["v", "verbose"]:
                s.verbose += 1
        elif parameter in ["V", "q", "quiet", "no-verbose"]:
                s.verbose = -1
        elif parameter in ["p", "play"]:
                s.midi_play = True
                info_enabled(s, "MIDI play")
        elif parameter in ["P", "no-play"]:
                s.midi_play = False
                info_disabled(s, "MIDI play")
        elif parameter in ["c", "compile"]:
                s.latex_compile = True
                info_enabled(s, "Compilation to pdf")
        elif parameter in ["C", "no-compile"]:
                s.latex_compile = False
                info_disabled(s, "Compilation to pdf")
        elif parameter in ["b", "show-bars"]:
                s.show_bars = True
                manually_defined.append("latex-showbars")
                info(s, 1, "Displaying bar lines")
        elif parameter in ["B", "no-show-bars"]:
                s.show_bars = False
                manually_defined.append("latex-showbars")
                info(s, 1, "Not displaying bar lines")
        elif parameter in ["k", "chords-text"]:
                s.show_chord, s.show_text = True, True
                manually_defined.append("latex-mode")
                info(s, 1, "Displaying both chords and text")
        elif parameter in ["K", "chords-only"]:
                s.show_text = False
                manually_defined.append("latex-mode")
                info(s, 1, "Not displaying text")
        elif parameter in ["T", "text-only"]:
                s.show_chords = False
                manually_defined.append("latex-mode")
                info(s, 1, "Not displaying chords")
        elif parameter in ["transpose"]:
                if not args.is_over():
                       value = int(args.get_arg())
                       s.global_transpose += value
                       info(s, 1, "Transposing by: ", str(value) + " semitones")
        elif parameter in ["key"]:
                if not args.is_over():
                       ask_key = note_from_string(args.get_arg())
                       info(s, 1, "Fixing key to: ", str(ask_key))
        elif parameter in ["style"]:
                #TODO
                pass
        elif parameter in ["template"]:
                #TODO
                pass
        elif parameter in ["o", "output"]:
                #TODO
                pass
        else:
                error(s, -1, "Invalid parameter " + parameter +". Try --help for more information.")
                exit()

class ArgsHandle:
        args = []
        counter = 0
        
        def __init__(self, args):
                self.args = args
                self.counter = 0
                
        def is_over(self):
                return self.counter >= len(self.args)
                
        def get_arg(self):
                if self.is_over():
                        return None
                else:
                        arg = self.args[self.counter]
                        self.counter += 1
                        return arg

        def is_parameter(self):
                if self.is_over():
                        return True
                elif self.args[self.counter][0] == "-":
                        return True
                else:
                        return False
                        
        def get_until_param(self):
                output = ""
                while not self.is_parameter():
                        output += self.get_arg()
                return output

if len(argv)<2:
        error(s, -1, "No argument was given. Try --help for advice")
        exit()
else:
        args = ArgsHandle(argv[1:])
        
        while not args.is_over():
                s.song_file = args.get_until_param()
                if not args.is_over():
                        arg = args.get_arg()
                        
                        if arg[:2] == "--":
                                handle_parameters(arg[2:], args, s)
                        elif arg[0] == "-":
                                for char in arg[1:]:
                                        handle_parameters(char, args, s)

        s.song_name = basename(s.song_file)
        if s.song_name.lower()[-5:] != ".song":
                error(s, -1, "Not a .song file. Try --help for advice")
                exit()
        else:
                s.song_name = s.song_name[:-5]

info(s, 1, "Parsing song file: ", s.song_file)
song = parse_songfile(s)

if "latex-mode" in song.metadata and "latex-mode" not in manually_defined:
        latex_mode = song.metadata["latex-mode"].lower()
        if latex_mode in ["chords-text", "default"]:
                s.show_chords, s.show_text = True, True
        elif latex_mode == "chords-only":
                s.show_chords, s.show_text = True, False
        elif latex_mode == "text-only":
                s.show_chords, s.show_text = False, True

if "latex-showbars" in song.metadata and "latex-showbars" not in manually_defined:
        latex_showbars = song.metadata["latex-showbars"].lower()
        if latex_showbars in ["off", "default"]:
                s.show_bars = False
        elif latex_showbars == "on":
                s.show_bars = True

if "global-transpose" in song.metadata:
        s.global_transpose += int(song.metadata["global-transpose"])
        
if "key" in song.metadata and ask_key != None:
        old_key = note_from_string(song.metadata["key"])
        difference = ask_key.pitch - old_key.pitch
        s.global_transpose += difference
        song.metadata["key"] = str(ask_key)
        info(s, 1, "Key was " + str(old_key) + ". Transposing by " + str(difference) + " semitones")

if s.export_mode == EXPORT_MODE_TEX:
        s.output_folder = LATEX_DEFAULT_FOLDER
        s.pdf_output_folder = LATEX_PDF_DEFAULT_FOLDER
        maketex(song, s)
elif s.export_mode == EXPORT_MODE_MID:
        s.output_folder = MIDI_DEFAULT_FOLDER
        makemid(song, s)
elif s.export_mode == EXPORT_MODE_SONG:
        s.output_folder = SONG_DEFAULT_FOLDER
        makesong(song, s)
elif s.export_mode == EXPORT_MODE_TXT:
        s.output_folder = TXT_DEFAULT_FOLDER
        maketxt(song, s)

