from constants import *
from messages import *
from latex_export.separate_lines import separate_lines

def set_string_width(string, width, filler_char):
        return (string + filler_char*width)[:width]

def generate_txt_single_line(line, s):
        """ Input is a tuple (ends_with_bar_end, text_and_chords)
            where text_and_chords is a list of tuples:
                        (str chord_text, float duration, str text, bool split, bool delay, beginning_of_bar)
            Output is a tuple containing two lines of text. """

        ends_with_bar_end, text_and_chords = line
        
        chord_str = ""
        text_str = ""
        
        last_chord_was_split = True
        
        for chord_text, duration, text, split, delay, beginning_of_bar in text_and_chords:
                if s.show_bars and beginning_of_bar:
                        barline_text = "|" + (" " if last_chord_was_split else "")
                        chord_str += barline_text
                        text_str += barline_text

                last_chord_was_split = split

                text = text.replace("\\pth", "")

                if text == "":
                        text = " "                      # one space to avoid consecutive bar lines

                delay_length = 3
                # Alternative option: 
                # delay_length = len(chord_text)
                
                if delay:
                        text = " "*delay_length + text

                chord_text = chord_text + " "           # to avoid "colliding chords"

                filler_char = " "
                if split:
                        text += " "
                else:
                        filler_char = "-"               # if chord_text is too long, this will put "-"'s in the broken word

                col_width = max(len(chord_text), len(text))
                text, chord_text = set_string_width(text, col_width, filler_char), set_string_width(chord_text, col_width, " ")

                chord_str += chord_text
                text_str += text

        chord_str += "|" if ends_with_bar_end and s.show_bars else ""
        text_str += "|" if ends_with_bar_end and s.show_bars else ""

        return chord_str, text_str


def generate_txt_song(song, s):
        output = ""
        for label in ["title", "artist", "composer", "author", "year", "key"]:
                if label in song.metadata:
                        output += label.capitalize() + ": " + song.metadata[label] + "\n"

        output += "\n"

        for section_name in song.structure:
                output += "[" + section_name + "]\n"
                section = song.sections[section_name]
                for line in separate_lines(section, s, latex=False):
                        chord_str, text_str = generate_txt_single_line(line, s)
                        if chord_str.replace("|", "").strip() != "":
                                output += chord_str + "\n"
                        if text_str.replace("|", "").strip() != "":
                                output += text_str + "\n"
                output += "\n"

        return output

def maketxt(song, s):
        info(s, 1, "Generating text")
        output = generate_txt_song(song, s)

        output_path = s.output_folder + s.song_name + ".txt"
        info(s, 1, "Writing text to file: ", output_path)
        output_file = open(output_path, "w")
        output_file.write(output)
        output_file.close()
