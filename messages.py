from os import get_terminal_size

### CUSTOM CLI COLORS ###

class colors:
        HEADER = '\033[95m'
        OKBLUE = '\033[94m'
        OKCYAN = '\033[96m'
        OKGREEN = '\033[92m'
        WARNING = '\033[93m'
        FAIL = '\033[91m'
        ENDC = '\033[0m'
        BOLD = '\033[1m'
        UNDERLINE = '\033[4m'

def print_twocols(col1, col2, width1, stylecol1=""):
        width2 = get_terminal_size().columns - width1
        if width2 <= 1:
                width2 = 1
        fstcol = stylecol1 + (str(col1) + " "*width1)[:width1] + ("" if stylecol1 == "" else colors.ENDC)
        sndcol = str(col2)
        while sndcol.strip() != "" or fstcol.strip() != "":
                cut_index = width2
                
                if len(sndcol) >= width2:
                        # Find the last space in sndcol
                        index = -1
                        subcol = sndcol[:width2]
                        for i in range(len(subcol)):
                                if sndcol[len(subcol)-1-i] == " ":
                                        index = len(subcol)-1-i
                                        break
                                        
                        if index != -1 and index != 0:         # no space
                                cut_index = index
                
                print(fstcol + sndcol[:cut_index])
                
                if cut_index < len(sndcol) and sndcol[cut_index] == " ":
                        sndcol = sndcol [1:]
                sndcol = sndcol[cut_index:]
                fstcol = " "*(width1)

def print_minisong_code(s, command, arg):
        """ Displays minisong code for debugging. """
        if s.verbose >= 3:
                i = len(command)
                print_twocols(command, arg, 15, colors.OKBLUE)

def info(s, verboseness_level, string, arg = ""):
        if s.verbose >= verboseness_level:
                print(colors.BOLD + colors.OKGREEN + "[INFO] " + colors.ENDC + string + colors.BOLD + arg + colors.ENDC + ".")

def info_export_mode(s, export_mode):
        info(s, 1, "Export mode set to ", export_mode)
        
def info_enabled(s, string):
        info(s, 1, string + " enabled")
        
def info_disabled(s, string):
        info(s, 1, string + " disabled")
        
def warning(s, verboseness_level, string, arg = ""):
        if s.verbose >= verboseness_level:
                print(colors.BOLD + colors.WARNING + "[WARNING] " + colors.ENDC + string + colors.BOLD + arg + colors.ENDC + ".")
        
def error(s, verboseness_level, string, arg = ""):
        if s.verbose >= verboseness_level:
                print(colors.BOLD + colors.FAIL + "[ERROR] " + colors.ENDC + string + colors.BOLD + arg + colors.ENDC + ".")
