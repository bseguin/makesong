from constants import *

def separate_lines(section, s, latex=False):
        """ Generates lines, i.e. tuples (ends_with_bar_end, text_and_chords)
            where text_and_chords is a list of tuples:
                        (str chord_text, float duration, str text, bool split, bool delay, beginning_of_bar) """
        
        beginning_of_bar = True
        bars = section.bars
        text_and_chords = []
        
        for bar in bars:
                beginning_of_bar = True
                for textchord in bar.content:
                        split = textchord.split
                        delay = textchord.delay
                        text = textchord.text
                        chord_text = textchord.chord.transpose(s.global_transpose).to_string(latex)
                        
                        index = text.find(NEWLINE_MARKER)         # Handle multiline chords
                        ana_text, ana_split = "", False
                        if index != -1:
                                text, ana_text = text.split(NEWLINE_MARKER)[:2]
                                ana_split, split = split, True
                                
                        if not s.show_text:
                                text, ana_text = "", ""
                                split, delay = True, False
                                
                        if not s.show_chords:
                                chord_text = ""
                                
                        text_and_chords.append((chord_text, textchord.duration, text, split, delay, beginning_of_bar))
                        
                        if index != -1:
                                ends_with_bar_end = ( textchord == bar.content[-1] ) and ana_text == ""
                                yield (ends_with_bar_end, text_and_chords)
                                text_and_chords = [("", 0.0, ana_text, ana_split, False, False)] if ana_text.strip() != "" else []
                                
                        beginning_of_bar = False

        if text_and_chords != []:
                yield (True, text_and_chords)
