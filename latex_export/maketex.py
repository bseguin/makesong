from os import system

from constants import *
from messages import *
from latex_export.separate_lines import separate_lines

def generate_tex_single_line(line, s):
        """ Input is a tuple (ends_with_bar_end, text_and_chords)
            where text_and_chords is a list of tuples:
                        (str chord_text, float duration, str text, bool split, bool delay, beginning_of_bar)
            Output is LaTeX code. """

        ends_with_bar_end, text_and_chords = line
        
        head_str = "\\begin{tabular}{"
        chord_str = "\\redrow "
        text_str = "\\blackrow "
        bot_str = "\\end{tabular} \n \n "

        beginning_of_line = True
        
        for chord_text, duration, text, split, delay, beginning_of_bar in text_and_chords:
                if s.show_bars and beginning_of_bar:
                        head_str += " | "
                if beginning_of_line:
                        head_str += " = "
                head_str += " +l "
                
                chord_str += chord_text + " ~ "
                if delay:
                        text_str += " \phantom{" + chord_text + "}"
                text_str += text
                if split:
                        text_str += " ~"
                else:
                        text_str += "\Vhrulefill"
                chord_str += " & "
                text_str += "& "
                beginning_of_line = False
                
        head_str += (" | " if s.show_bars and ends_with_bar_end else " ") + " l} \n"
        chord_str += "\\\\ \n"
        text_str += " \n"
        final_str = head_str + chord_str + text_str + bot_str
        
        return final_str

def generate_tex_song(song, template, s):
        output = template
        
        for label in METADATA:
                if label in song.metadata:
                        output = output.replace("!"+label, song.metadata[label])
                else:
                        output = output.replace("!"+label, "")

        if "latex-cols" in song.metadata:
                latexcols = int(song.metadata["latex-cols"])
                if latexcols > 1:
                        output = output.replace("%cols-begin", "\\begin{multicols}{" + str(int(latexcols)) + "}")
                        output = output.replace("%cols-end", "\\end{multicols}")

        song_str = ""
        for section_name in song.structure:
                song_str += "\\marginpar{" + section_name + "} \n \n \\begin{flushleft} \n \n"
                section = song.sections[section_name]
                for line in separate_lines(section, s, latex=True):
                        song_str += generate_tex_single_line(line, s)
                song_str += "\n \n \\end{flushleft} \n \n"
        output = output.replace("!song", song_str)
        return output

def maketex(song, s):
        info(s, 1, "Importing LaTeX template: ", s.latex_template)
        template_file = open(s.latex_template, "r")
        template = template_file.read()
        template_file.close()

        info(s, 1, "Generating LaTeX code")
        output = generate_tex_song(song, template, s)

        output_path = s.output_folder + s.song_name + ".tex"
        info(s, 1, "Writing LaTeX code to file: ", output_path)
        output_file = open(output_path, "w")
        output_file.write(output)
        output_file.close()

        if s.latex_compile:
                info(s, 1, "Compiling LaTeX file to pdf file: ", s.pdf_output_folder + s.song_name + ".pdf")
                system("cd " + s.output_folder + " && "+ LATEX_COMPILE_COMMAND + s.song_name + ".tex")
                system("mv " + s.output_folder + s.song_name + ".pdf " + s.pdf_output_folder)
