from constants import *

""" This file defines the most important data types used for storing information about the song:
        The hierarchy is the following:
        - Note                                  A musical note, including its pitch (semitones away from middle C) and its "name" (i.e. the letter that describes it).
        - Chord                                 A chord, understood as: two notes (bass, root) and a descriptor of the chord nature.
                                                    Custom chord types are not yet supported.
        - TextChord                             A "text-chord" is made of a single chord + the text sung alongside this chord.
                                                    Multiple textchords make a bar
        - Bar                                   A musical bar, including its tempo, swing ratio and time signature.
        - Section                               A section, i.e. a list of bars.
        - Song                                  The song, i.e. metadata + a dictionary of sections
                                                    + a "structure" specifying the playing order of sections.

        We also define classes Instrument and Percussion, which are used for playback.                                              """

class Note():
        name = 0
        pitch = 0

        def __init__(self, name, pitch):
                self.name, self.pitch = name, pitch

        def compute_alterations(self):
                natural_pitch = NOTE_SEMITONES[self.name % 7]
                difference_pitch = (self.pitch - natural_pitch) % 12
                if difference_pitch > 7:
                        difference_pitch -= 12
                return difference_pitch

        def to_string(self, latex=False):
                global NOTE_NAMES
                sharp = "#" if not latex else "$\\sharp$"
                flat = "b" if not latex else "$\\flat$"
                alt = self.compute_alterations()
                if alt>=0:
                        return NOTE_NAMES[self.name%7] + (sharp * alt)
                else:
                        return NOTE_NAMES[self.name%7] + (flat * (-alt))

        def __str__(self):
                return self.to_string()
                
        def is_same(self, other):
                return self.name % 7 == other.name % 7 and self.pitch % 12 == other.pitch % 12

        def transpose(self, semitones):
                new_name = (self.name + TRANSP_DEG_BY_SEMI[semitones % 12]) % 7
                new_pitch = (self.pitch + semitones) % 12
                return Note(new_name, new_pitch)

def note_from_string(note):
        note = note.upper()
        name = NOTE_NAMES.index(note[0])
        alter = 0
        for i in range(1, len(note)):
                if note[i] == "#" or i<=len(note)-len("$\\SHARP$") and note[i:i+len("$\\sharp$")] == "$\\SHARP$":
                        alter += 1
                elif note[i] == "B" or i<=len(note)-len("$\\FLAT$") and note[i:i+len("$\\flat$")] == "$\\FLAT$":
                        alter -= 1
        pitch = (NOTE_SEMITONES[name]+alter)%12
        return Note(name, pitch)

class Chord():
        special = CHORD_UNDEF
        root = None             # Note
        nature = ""             # str
        bass = None             # Note

        def __init__(self, chord_text=""):
                Uchord_text = chord_text.upper().strip()

                if Uchord_text in ["", "."]:
                        self.special = CHORD_UNDEF
                elif Uchord_text == "N":
                        self.special = CHORD_NC
                elif Uchord_text in ["\\%", "%"]:
                        self.special = CHORD_REPEAT
                else:
                        self.special = CHORD_NORMAL

                        bass = None
                        if "/" in chord_text:
                                pieces = chord_text.split("/")
                                bass = note_from_string(pieces[1])
                                chord_text = pieces[0]

                        if Uchord_text[0] not in NOTE_NAMES:
                                return None

                        i = 1
                        while i < len(Uchord_text) and Uchord_text[i] in ["#", "B"]:
                                i += 1
                        root_text = Uchord_text[0:i]
                        nature = chord_text[i:]
                        root = note_from_string(root_text)
                        if bass == None:
                                bass = root
                        
                        self.root, self.nature, self.bass = root, nature, bass

        def to_string(self, latex=False):
                if self.special == CHORD_NC:
                        return "N.C." if latex else "N"
                elif self.special == CHORD_UNDEF:
                        return ""
                elif self.special == CHORD_REPEAT:
                        return "\\%" if latex else "%"
                if self.root == None or self.bass == None:
                        return ""
                nature = self.nature
                if latex:
                    nature = nature.replace("b","$\\flat$").replace("#","$\\sharp$")
                result = self.root.to_string(latex) + nature
                if not self.root.is_same(self.bass):
                        result += "/" + self.bass.to_string(latex)
                return result

        def __str__(self):
                return self.to_string()

        def transpose(self, semitones):
                new_chord = Chord()
                new_chord.special = self.special
                new_chord.nature = self.nature
                if self.special == CHORD_NORMAL:
                        new_chord.root = self.root.transpose(semitones)
                        new_chord.bass = self.bass.transpose(semitones)
                return new_chord


class TextChord():
        """ A "text-chord" is the minimal object containing both harmony and lyrics,
            it is made a of a single chord + the corresponding text underneath.      """
        text = ""
        chord = None            # Chord
        delay = False           #                               !delay <=> sing on the chord
        split = True            #                               !split <=> the last word doesn't overlap with the next chord change
        duration = 4            #                               In quarter notes

        def duration_unspecified(self):
                return not isinstance(self.duration, float)
                # return duration == "" or duration == "?" or duration == None

class Bar():
        content = None          # TextChord[]
        duration = 4            # In quarter notes (TODO: store entire time sig instead)
        tempo = 100
        swing = 0.5

        def __init__(self, duration, tempo, swing):
                self.duration, self.tempo, self.swing = duration, tempo, swing
                self.content = []

        def compute_durations(self):
                nb_txch = len(self.content)

                if nb_txch == 0:
                        return
                if nb_txch == 1:
                        self.content[0].duration = self.duration
                        return

                indices_unspec = []
                sum_spec = 0
                for k in range(nb_txch):
                        txch = self.content[k]
                        if txch.duration_unspecified():
                                indices_unspec.append(k)
                        else:
                                sum_spec += txch.duration

                if len(indices_unspec) == 0:
                        return

                remaining = (self.duration - sum_spec)/len(indices_unspec)
                for i in indices_unspec:
                        self.content[i].duration = remaining                        

        def is_zero(self):
                return ((isinstance(self.duration, float) and self.duration <= 0.01)
                        or (isinstance(self.duration, str) and self.duration.strip() == "0")
                        or (isinstance(self.duration, str) and self.duration.strip() == "0.0")
                        or (isinstance(self.duration, int) and self.duration == 0))

class Section():
        bars = None            # Bar[]

        def __init__(self):
                self.bars = []

        def text_chords(self):
                if self.bars == None or len(self.bars)==0:
                        yield from []
                else:
                        for bar in self.bars:
                                yield from bar.content

class Song():
        metadata = None         # dict : str => str             Information about the song
        sections = None         # dict : str => Section         Sections by label
        structure = None        # str[]                         Labels of the sections in playing order

        def __init__(self):
                self.metadata, self.sections, self.structure = {}, {}, []

        def get_chords(self):
                """ Yields triples of the form (time, time_in_bar, chord, tempo, swing) """
                time = 0
                for i in self.structure:
                        section = self.sections[i]
                        for bar in section.bars:
                                time_in_bar = 0
                                for textchord in bar.content:
                                        yield (time, time_in_bar, textchord.chord, bar.tempo, bar.swing)
                                        chord_duration = 4
                                        if(textchord.duration != "?"):
                                                chord_duration = float(textchord.duration)
                                        time += chord_duration
                                        time_in_bar += chord_duration

class Instrument:
        name = "Piano"
        patterns = None                                 # cf. the file "readpattern.py" for a description of the format of patterns
        midi_id = 0
        octave_shift = 0
        volume = 100
        is_perc = False

        def __init__(self, name, midi_id, patterns, volume=100, octave_shift = 0):
                self.name, self.patterns, self.midi_id, self.volume, self.octave_shift = name, patterns, midi_id, volume, octave_shift

class Percussion(Instrument):
        name = "Drum"
        is_perc = True

        def __init__(self, name, patterns, volume=100):
                self.name, self.patterns, self.volume = name, patterns, volume
